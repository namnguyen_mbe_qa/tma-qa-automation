package com.tma.mbe.google;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.tma.mbe.utilities.DataDriven;
import com.tma.mbe.utilities.Lib;

public class Calculator {

	public static WebDriver driver;
	String ChromeDriver = "src\\main\\resources\\chromedriver.exe";
	String locationPath = "src\\main\\resources\\location.xls";
	String dataDrivenPath = "src\\main\\resources\\datadriven.xls";
	String sheetName = "GoogleSearch";
	String tableName1 = "Google_Calculator_Plus";
	String tableName2 = "Google_Calculator_Multiply";
	String baseUrl = "http://google.com";

	@BeforeClass
	public void prepare() {
		Lib.locaton = DataDriven
				.getSheetProperty(locationPath, sheetName);
	}

	@Parameters({"appiumServer", "udid"})
	@BeforeMethod
	public void init(String appiumServer, String udid) {

		/*
		System.setProperty("webdriver.chrome.driver", ChromeDriver);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		*/

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "Android Emulator");
		//capabilities.setCapability("platformVersion", "4.4.2");
		capabilities.setCapability("newCommandTimeout", "180");
		capabilities.setCapability("udid", udid);

		try {
			driver = new RemoteWebDriver(
					new URL(appiumServer), capabilities);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		

	}

	@AfterMethod
	public void tearDown() {
		ArrayList<String> tabs = new ArrayList<String>(
				driver.getWindowHandles());
		if (tabs.size() > 1) {
			driver.close();
			driver.switchTo().window(tabs.get(0));
		}
		driver.quit();
	}

	@AfterClass
	public static void end() {
		//driver.quit();
	}

	@DataProvider(name = "Google_Calculator_Plus")
	public Object[][] createData1() throws Exception {
		Object[][] retObjArr = DataDriven.getTableArray(dataDrivenPath,
				sheetName, tableName1);
		return (retObjArr);
	}

	@DataProvider(name = "Google_Calculator_Multiply")
	public Object[][] createData2() throws Exception {
		Object[][] retObjArr = DataDriven.getTableArray(dataDrivenPath,
				sheetName, tableName2);
		return (retObjArr);
	}

	/**
	 * verify a+b=c
	 */
	@Test(groups = { "GoogleCalculator" }, dataProvider = "Google_Calculator_Plus")
	public void Google_Calculator_Plus(String a, String b, String result)
			throws Exception {

		driver.get(baseUrl);

		WebElement searchBox = driver.findElement(By.xpath(Lib.locaton
				.getProperty("searchBox")));
		Assert.assertTrue(searchBox.isDisplayed(),
				"Google search box is not visible");
		searchBox.sendKeys(a + "+" + b);

		WebElement searchButton = driver.findElement(By.xpath(Lib.locaton
				.getProperty("searchButton")));
		searchButton.click();

		WebElement resultBox = driver.findElement(By.xpath(Lib.locaton
				.getProperty("resultBox")));
		resultBox.isDisplayed();
		Thread.sleep(2000);

		// verify the result
		Assert.assertEquals(resultBox.getText(), result);
	}

	/**
	 * verify a*b=c
	 */
	@Test(groups = { "GoogleCalculator" }, dataProvider = "Google_Calculator_Multiply")
	public void Google_Calculator_Multiply(String a, String b, String result)
			throws Exception {

		driver.get(baseUrl);

		WebElement searchBox = driver.findElement(By.xpath(Lib.locaton
				.getProperty("searchBox")));
		Assert.assertTrue(searchBox.isDisplayed(),
				"Google search box is not visible");
		searchBox.sendKeys(a + "*" + b);

		WebElement searchButton = driver.findElement(By.xpath(Lib.locaton
				.getProperty("searchButton")));
		searchButton.click();

		WebElement resultBox = driver.findElement(By.xpath(Lib.locaton
				.getProperty("resultBox")));
		resultBox.isDisplayed();
		Thread.sleep(2000);

		// verify the result
		Assert.assertEquals(resultBox.getText(), result);
	}
}
