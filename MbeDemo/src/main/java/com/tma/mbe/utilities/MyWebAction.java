package com.tma.mbe.utilities;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyWebAction {

	private static WebDriver driver;
	public static Logger testLogs = Logger.getLogger(MyWebAction.class
			.getName());

	public MyWebAction(WebDriver driver) {
		this.driver = driver;
	}

	public static void main(String[] args) {
		assertElementExist("id=abc");
	}

	public static void assertElementExist(String elementName) {
		testLogs.debug("[info] Executing: assertElementExist " + elementName
				+ " object");
		// assertTrue(!driver.findElements(item).isEmpty(), "The " + elementName
		// + " doesn't exist");
		// new
		// Actions(driver).moveToElement(driver.findElement(item)).perform();
	}

	public static void assertElementIsNotExist(String elementName, By item) {
		assertFalse(!driver.findElements(item).isEmpty(), "The " + elementName
				+ " still exists");
	}

	public static void assertElementIsVisible(String elementName, By item) {
		// assertElementExist(elementName, item);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(item));
		assertTrue(driver.findElement(item).isDisplayed(), "The " + elementName
				+ " isn't visible");
	}

	// public static void VerifyText(String elementName, By item, String text) {
	// assertElementIsVisible(elementName, item);
	// assertEquals(driver.findElement(item).getText().trim(), equalTo(text));
	// }

	public static String getAttribute(String elementName, By item,
			String attributeName) {
		assertElementIsVisible(elementName, item);
		return driver.findElement(item).getAttribute(attributeName);
	}

	public static void clickAnElement(String elementName, By item) {
		assertElementIsVisible(elementName, item);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(item));
		driver.findElement(item).click();
	}

	public static void sendKeys(String elementName, By item, String key) {
		assertElementIsVisible(elementName, item);
		driver.findElement(item).clear();
		driver.findElement(item).sendKeys(key);
	}

	public static String closeAlertAndGetItsText(boolean acceptNextAlert) {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public static void switchToAnotherTab(int numberOfWindows) {
		driver.switchTo().window(
				(new ArrayList<String>(driver.getWindowHandles())
						.get(numberOfWindows - 1).toString()));
	}

	public static void clickAnElementSafariNewTab(String elementName, By item) {
		assertElementIsVisible(elementName, item);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(item));
		String link = driver.findElement(item).getAttribute("href");
		driver.get(link);
	}

	public static boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	/* close mobile-bookmark-bubble */
	public static void closeBubble() {
		// assertElementExist("bookmark_bubble.min.js script",
		// By.xpath("//script[contains(@src, 'bookmark_bubble.min.js')]"));
		((JavascriptExecutor) driver)
				.executeScript("google.bookmarkbubble.Bubble.prototype.destroy()");
		((JavascriptExecutor) driver)
				.executeScript("window.localStorage['BOOKMARK_DISMISSED_COUNT'] = 2;");
	}

	public static void printPageSource(String filePath) {
		PrintWriter pageSource;
		wait(5000);
		try {
			pageSource = new PrintWriter(filePath, "UTF-8");
			pageSource.print(driver.getPageSource());
			pageSource.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * private static BufferedImage getScaledImage( BufferedImage originalImage,
	 * int type) { int height = (int) (originalImage.getHeight() / 1); int width
	 * = (int) (originalImage.getWidth() / 1); BufferedImage resizedImage = new
	 * BufferedImage(width, height, type); Graphics2D g =
	 * resizedImage.createGraphics(); g.drawImage(originalImage, 0, 0, width,
	 * height, null); g.dispose(); g.setComposite(AlphaComposite.Src);
	 * g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	 * RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	 * g.setRenderingHint(RenderingHints.KEY_RENDERING,
	 * RenderingHints.VALUE_RENDER_QUALITY);
	 * g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	 * RenderingHints.VALUE_ANTIALIAS_ON);
	 * 
	 * return resizedImage; }
	 */

	public static void TakeScreenShot(String screenshotName) {

		WebDriver dr = new Augmenter().augment(driver);
		File srcFile = ((TakesScreenshot) dr).getScreenshotAs(OutputType.FILE);

		try {
			BufferedImage bufferedImage = ImageIO.read(srcFile);
			/*
			 * int type = bufferedImage.getType() == 0 ?
			 * BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();
			 * 
			 * if (bufferedImage.getWidth() > 1080) bufferedImage =
			 * getScaledImage(bufferedImage, type);
			 */
			ImageIO.write(bufferedImage, "png", srcFile);
			File file = new File("report/ScreenShot/" + screenshotName + ".png");
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			FileUtils.copyFile(srcFile, file, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void wait(int milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void ClearAllCookie() {
		driver.manage().deleteAllCookies();
		wait(3000);
	}
}
