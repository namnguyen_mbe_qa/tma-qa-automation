package com.tma.mbe.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.openqa.selenium.By;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author Hiep Pham This class is excel interaction library
 */
public class MyDataDriven {

	public static void main(String[] args) throws Exception {
	}

	/**
	 * This method converts from excel data to Array[][]
	 */
	public static String[][] getTableArray(String xlsFilePath,
			String sheetName, String tableName) throws Exception {

		String[][] tabArray = null;
		Workbook workbook;
		Sheet sheet = null;
		Cell tableStart = null;

		try {
			workbook = Workbook.getWorkbook(new File(xlsFilePath));
			sheet = workbook.getSheet(sheetName);
			tableStart = sheet.findCell(tableName);
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int startRow, startCol, endRow, endCol, ci, cj;

		startRow = tableStart.getRow();
		startCol = tableStart.getColumn();

		Cell tableEnd = sheet.findCell(tableName, startCol + 1, startRow + 1,
				100, 64000, false);

		endRow = tableEnd.getRow();
		endCol = tableEnd.getColumn();

		tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
		ci = 0;

		for (int i = startRow + 1; i < endRow; i++, ci++) {
			cj = 0;
			for (int j = startCol + 1; j < endCol; j++, cj++) {
				tabArray[ci][cj] = sheet.getCell(j, i).getContents();
			}
		}
		return (tabArray);
	}
}
