package com.tma.mbe.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.openqa.selenium.By;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * @author Hiep Pham This class is excel interaction library
 */
public class MyLocation {

	public static void main(String[] args) throws Exception {
	}

	public static By stringToBy(String str) {
		By by = null;
		if (str.startsWith("id=")) {
			by = By.id(str.substring(3));
		} else if (str.startsWith("name=")) {
			by = By.name(str.substring(5));
		} else if (str.startsWith("css=")) {
			by = By.cssSelector(str.substring(4));
		} else if (str.startsWith("link=")) {
			by = By.linkText(str.substring(5));
		} else if (str.startsWith("xpath=")) {
			by = By.xpath(str.substring(6));
		} else
			System.out.println("[>>> ERROR <<<] Executing: | Not support "
					+ str + " location |");
		return by;
	}

	/**
	 * This method converts from excel data to Property
	 * */
	public static Properties getSheetProperty(String xlsFilePath,
			String sheetName) {

		Properties property = new Properties();
		Sheet sheet = null;
		Workbook workbook = null;
		OutputStream output = null;

		try {
			workbook = Workbook.getWorkbook(new File(xlsFilePath));
			sheet = workbook.getSheet(sheetName);
		} catch (BiffException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		int startRow, endRow;
		startRow = 1;
		endRow = sheet.getRows();

		try {
			output = new FileOutputStream("location.properties");
			for (int i = startRow; i < endRow; i++) {
				property.setProperty(sheet.getCell(0, i).getContents(), sheet
						.getCell(1, i).getContents());
			}
			property.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return property;
	}
}
