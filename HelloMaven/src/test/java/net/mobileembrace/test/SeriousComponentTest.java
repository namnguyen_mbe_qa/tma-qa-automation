package net.mobileembrace.test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SeriousComponentTest {

  @Test(groups={"unit"})
  public void testSad() throws Exception {
    assert SeriousComponent.testSeriousness("SAD");
  }

  @Test(groups={"unit"})
  public void testSerious() throws Exception {
    assert SeriousComponent.testSeriousness("SERIOUS");
  }

  @Test(groups={"unit"})
  public void testCrazy() throws Exception {
    assert SeriousComponent.testSeriousness("CRAZY");
  }

  @Test(groups={"unit"})
  public void testFunny() throws Exception {
    assert !SeriousComponent.testSeriousness("FUNNY");
  }

  @Parameters({"databaseHostname"})
  @Test(groups={"integration"})
  public void testLargeFile(String databaseHostname) throws Exception {
   // String text = "TEST";
    // Imagine that this method contained some serious
    // code that loaded a 100k line text file and
    // tested each line.
    System.out.println( "Database Hostname: " + databaseHostname );

    assert SeriousComponent.testSeriousness(databaseHostname);
    
  }
}